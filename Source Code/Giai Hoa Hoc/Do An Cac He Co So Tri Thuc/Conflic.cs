﻿//--------------------------------------------------------------------------------------------------
// Final project - Knowledge base systems
// Solving chain reactions
// Course's ID: CS217.H21.KHTN
// Lecturers: Assoc Prof. Do Van Nhon - Msc. Huynh Thi Thanh Thuong
// Authors: Do Phu An (14520002) - Luu Thanh Son (14520772).
//--------------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Do_An_Cac_He_Co_So_Tri_Thuc
{
    class Conflic
    {
        List<String> left_side;
        List<String> right_side;
        public Conflic(List<String>left, List<String> right)
        {
            this.left_side = left;
            this.right_side = right;
        }
        //Bỏ đi những gốc trùng nhau ở 2 vế trái và phải
        public void removeConflic()
        {
            for (int i = 0; i < left_side.Count; i++)
            {
                for (int j = 0; j < right_side.Count; j++)
                    if (left_side[i] == right_side[j])
                    {
                        left_side.RemoveAt(i);
                        right_side.RemoveAt(j);
                        i = 0;
                        j = 0;
                        if (left_side.Count == 0 || right_side.Count == 0)
                            break;
                        else
                            continue;
                    }
                if (left_side.Count == 0)
                    break;
            }
        }
        public List<String>getLeft()
        {
            return this.left_side;
        }
        public List<String>getRight()
        {
            return this.right_side;
        }
    }
}
