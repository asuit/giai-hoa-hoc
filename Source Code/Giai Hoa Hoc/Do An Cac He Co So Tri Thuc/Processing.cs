﻿//--------------------------------------------------------------------------------------------------
// Final project - Knowledge base systems
// Solving chain reactions
// Course's ID: CS217.H21.KHTN
// Lecturers: Assoc Prof. Do Van Nhon - Msc. Huynh Thi Thanh Thuong
// Authors: Do Phu An (14520002) - Luu Thanh Son (14520772).
//--------------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Do_An_Cac_He_Co_So_Tri_Thuc
{
    // This class is use for running all steps in our program. 
    // Input: a String of chain reaction.
    //  - Rule imported from file. 
    // Call getResult function to execute this program. 

    class Processing
    {
        String input;
        List<Rule> rules;
        List<String> reaction = new List<String>();
        public Processing(String input,List<Rule>Rule)
        {
            this.rules = Rule;
            this.input = input;
        }

        public Processing() { }

        public void setInput(String input) { this.input = input; }

        public void setRule(List<Rule>Rule) { this.rules = Rule; }

        public void execute()
        {
            List<String>formulars = Pair.getAllFormula(this.input);
            List<Pair> pairs = Pair.makePairs(formulars);

            foreach (Pair pair in pairs)
            {
                Element firstElement = new Element(pair.str1);
                Element lastElement = new Element(pair.str2);
                GetReaction getReact = new GetReaction();
                getReact.set_ele(firstElement.getElementType(), lastElement.getElementType());
                List<String> react = new List<String>();
                react = getReact.getReaction(this.rules);
                if (react.Count >= 1)
                {
                    for (int j = 0; j < react.Count; j++)
                    {
                        Reaction re = new Reaction(react[j], pair.str1, pair.str2);
                        String t = re.getReaction();
                        if (t != "")
                        {
                            reaction.Add(t);
                            break;
                        }
                    }
                }
                else
                {
                    getReact.set_ele(pair.str1, pair.str2);
                    react = getReact.getReaction(this.rules);
                    if (react.Count >= 1)
                    {
                        for (int j = 0; j < react.Count; j++)
                        {
                            Reaction re = new Reaction(react[j], pair.str1, pair.str2);
                            String t = re.getReaction();
                            if (t != "")
                            {
                                reaction.Add(t);
                                break;
                            }
                        }
                    }
                }
            }

            //for (int i = 0; i < pairs.Count-1; i++)
            //{
            //    String first = pairs[i];
            //    String last = pairs[i + 1];

            //    Element firstElement = new Element(first);
            //    Element lastElement = new Element(last);

            //    GetReaction getReac = new GetReaction(firstElement.getElementType(), lastElement.getElementType(), this.rules);
            //    List<String> react = getReac.getReaction();

            //    if (react.Count >= 1) 
            //    {
            //        for (int j = 0; j < react.Count; j++) 
            //        {
            //            Reaction re = new Reaction(react[j], first, last);
            //            String t = re.getReaction();
            //            if (t != "")
            //            {
            //                reaction.Add(t);
            //                break;
            //            }
            //        }
            //    }
            //}
        }

        public List<String>getResult()
        {
            execute();
            return this.reaction;
        }
    }
}
