﻿//--------------------------------------------------------------------------------------------------
// Final project - Knowledge base systems
// Solving chain reactions
// Course's ID: CS217.H21.KHTN
// Lecturers: Assoc Prof. Do Van Nhon - Msc. Huynh Thi Thanh Thuong
// Authors: Do Phu An (14520002) - Luu Thanh Son (14520772).
//--------------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Do_An_Cac_He_Co_So_Tri_Thuc
{
    class Pair
    {
        // This class is used for expand the string of chain reaction into a list of formulars. 
        // After that, group a group of 2 formular into a pair
        // str1: first element
        // str2: last element

        public string str1 { get; set; }
        public string str2 { get; set; }

        public Pair() { }

        public Pair(string v1, string v2)
        {
            str1 = v1;
            str2 = v2;
        }

        public static List<string> getAllFormula(string input)
        {
            List<string> res = new List<string>();
            string pattern = @"\w+((\((?:[^()]+|())*\)\d*))|\w+";
            Regex regx = new Regex(pattern, RegexOptions.IgnoreCase);

            MatchCollection matches = regx.Matches(input);
            foreach (Match match in matches)
            {
                res.Add(match.Value);
            }
            return res;
        }

        public static List<Pair> makePairs(List<string> input)
        {
            List<Pair> pairs = new List<Pair>();
            for (int i = 0; i < input.Count - 1; i++)
            {
                pairs.Add(new Pair(input.ElementAt(i), input.ElementAt(i + 1)));
            }
            return pairs;
        }
    }
}
