﻿//--------------------------------------------------------------------------------------------------
// Final project - Knowledge base systems
// Solving chain reactions
// Course's ID: CS217.H21.KHTN
// Lecturers: Assoc Prof. Do Van Nhon - Msc. Huynh Thi Thanh Thuong
// Authors: Do Phu An (14520002) - Luu Thanh Son (14520772).
//--------------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Do_An_Cac_He_Co_So_Tri_Thuc
{
    // Using forward reasoning to find suitable chain reactions.
    class GetReaction
    {
        String pre_Ele;
        String re_Ele;
        //List<Rule> rule;
        List<String> reaction = new List<string>();

        public GetReaction() { }
        public GetReaction(String pre, String re) //, List<Rule>r)
        {
            this.pre_Ele = pre;
            this.re_Ele = re;
            //this.rule = r;
        }
        
        // Get all suitable formulars
        public void processReaction(List<Rule> rule)
        {
            for (int i=0;i<rule.Count;i++)
            {
                if (rule[i].getHypos() == pre_Ele && rule[i].getFactor() == re_Ele)
                    reaction.Add(rule[i].getReaction());
            }
        }
        public List<String>getReaction(List<Rule> r)
        {
            processReaction(r);
            return this.reaction;
        }
        public void set_ele(String first,String last)
        {
            this.pre_Ele = first;
            this.re_Ele = last;
        }
    }
}
