﻿//--------------------------------------------------------------------------------------------------
// Final project - Knowledge base systems
// Solving chain reactions
// Course's ID: CS217.H21.KHTN
// Lecturers: Assoc Prof. Do Van Nhon - Msc. Huynh Thi Thanh Thuong
// Authors: Do Phu An (14520002) - Luu Thanh Son (14520772).
//--------------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Data.SqlClient;

namespace Do_An_Cac_He_Co_So_Tri_Thuc
{
    public partial class Search : Form
    {
        String path = "Database\\";
        private List<string> temp = new List<string>();
        private string[] lines;

        public Search()
        {
            InitializeComponent();
            if (searchTxtBox.Text == "")
            {
                tabPanel.Enabled = false;
            }
            getPath();
        }
        public void getPath()
        {
            FolderBrowserDialog openFileDialog = new FolderBrowserDialog();

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                // Import file  
                this.path = openFileDialog.SelectedPath + "\\";             
            }
        }
        private List<string> getElementsInfoFromSymbol(String symbol)
        {
            List<string> results = new List<string>();
            //List<string> sqls = new List<string>();

            SqlConnection conn = DatabaseConnect.GetDBConnection(ConnectString.getConnString());

            try
            {
                conn.Open();
                SqlCommand command = new SqlCommand();

                command.Connection = conn;
                string[] cols;
                string[] prefixs;
                if (symbol.Length <= 2)
                {
                    cols = new string[] { "TenGoi", "HoaTri", "NguyenTuKhoi", "DacDiem" };
                    prefixs = new string[] { "- Name: ", "- Oxidation states: ", "- Standard atomic weight: ", "- Features: " };
                }
                else
                {
                    cols = new string[] { "TenGoi", "NguyenTuKhoi", "DacDiem" };
                    prefixs = new string[] { "- Name: ", "- Standard atomic weight: ", "- Features: " };
                }
                
                for (int i = 0; i< cols.Length; i++)
                {
                    string cmdLine = "select " + cols[i] + " from ThongTin where KyHieu=" + "\'" + symbol + "\'";
                    command.CommandText = cmdLine;
                    string temp = command.ExecuteScalar().ToString();
                    results.Add(prefixs[i] + temp);
                }


                string sql = "select PicturePath from ThongTin where KyHieu=" + "\'" + symbol + "\'";
                command.CommandText = sql;
                try
                {
                    pictureBox.Image = Image.FromFile(path+command.ExecuteScalar().ToString());
                }
                catch (Exception e)
                {

                }
            }
            finally
            {
                conn.Close();
            }
            return results;
        }
        private void searchTxtBox_TextChanged(object sender, EventArgs e)
        {
            tabPanel.Enabled = searchTxtBox.Text == "" ? false : true;

        }

        private void searchTxtBox_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                featuresTxtBox.Text = "";
                List<string> results = new List<string>();
                results = getElementsInfoFromSymbol(searchTxtBox.Text);

                string[] res = results.ToArray();
                foreach (string r in res)
                {
                    //featuresTabPage.Text += r + Environment.NewLine;
                    featuresTxtBox.Text += r + Environment.NewLine;
                }
            }
        }
    }
}
