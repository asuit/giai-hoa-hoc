﻿//--------------------------------------------------------------------------------------------------
// Final project - Knowledge base systems
// Solving chain reactions
// Course's ID: CS217.H21.KHTN
// Lecturers: Assoc Prof. Do Van Nhon - Msc. Huynh Thi Thanh Thuong
// Authors: Do Phu An (14520002) - Luu Thanh Son (14520772).
//--------------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Do_An_Cac_He_Co_So_Tri_Thuc
{
    //Class xử lý việc tìm ra cac chất cụ thể vào phản ứng. Gọi hàm getReaction() là được. 
    class Reaction
    {
        List<String> left_corner;  //Các loai chất bên trái của phản ứng
        List<String> right_corner; //Các loại chất bên phải của phản ứng
        String recation;

        String first_ele;      //Chất đầu
        String last_ele;       //Chất cuối

        List<String> left_result;     //Chất cụ thể bên vế trái chương trình
        List<String> right_result;      //Chất cụ thể bên vế phải chương trình
        public Reaction(String rec,String first, String last)
        {
            this.recation = rec;
            left_corner = new List<String>();
            right_corner= new List<String>();
            left_result= new List<String>();
            right_result= new List<String>();
            this.first_ele = first;
            this.last_ele = last;
        }
        //Hàm này phân tích các vế của phản ứng ra vế trái và vế phải
        //Ví dụ: phản ứng KLK+O2=OXITB
        //Vế trái phản ứng: KLK,O2
        //Vế phải phản ứng: OXITB
        public void splitReaction()
        {
            int q = 0;
            bool left = true;
            String t = "";
            for(int i=0;i<recation.Length;i++)
            {
                //Có 4 trường hợp lấy Loại chất trong phản ứng
                //Đây chỉ là loại chất trong phản ứng, không phải phản ứng hoàn chỉnh
                //VD: KLK+O2=OXITB
                //Lấy đến dấu = Lúc này đánh dấu là đã xét hết vế trái
                if (recation[i]=='=')    
                { 
                    left = false;
                    t = recation.Substring(q, i - q);  
                    left_corner.Add(t);
                    q = i + 1;
                    continue;
                }
                //Lấy từng set cách nhau bởi dấu + Biến left cho biết đang xét vế trái hay vế phải. true là vế trái
                if (recation[i]=='+' && left==true) 
                {
                    t = recation.Substring(q, i - q);
                    left_corner.Add(t);
                    q = i + 1;
                }
                //Tương tự như trên nhưng đang xét vế phải
                if (recation[i] == '+' && left == false)   
                {
                    t = recation.Substring(q, i - q);
                    right_corner.Add(t);
                    q = i + 1;
                }

                //Lấy đến phần cuối của phương trình phản ứng
                if (i == recation.Length - 1)    
                {
                    t = recation.Substring(q, i - q+1);
                    right_corner.Add(t);
                    q = i + 1;
                }
            }
        }

        //Tiền xử lý các vế của phản ứng. Bỏ đi những chất đã có, chỉ xét những chất chưa có
        public void PreProcessing()
        {
            String[] ele = { "H2", "O2", "H2O", "NO2", "SO2", "NO" };  //Những chất đã có sẵn trong phản ứng. Bỏ đi những chất này
            for (int i = 0; i < 6; i++)
            {
                int a = left_corner.IndexOf(ele[i]);
                int b = right_corner.IndexOf(ele[i]);
                if (a != -1)
                {
                    left_corner.RemoveAt(a);
                    left_result.Add(ele[i]);
                }
                if (b != -1)
                {
                    right_corner.RemoveAt(b);
                    right_result.Add(ele[i]);
                }
            }
            List<String> type_ele = Element.getAllElementType();
            for (int i = 0; i < left_corner.Count; i++) 
            {
                if (!type_ele.Contains(left_corner[i]))
                {
                    left_result.Add(left_corner[i]);
                    left_corner.RemoveAt(i);
                    i = 0;
                    if (left_corner.Count == 0)
                        break;
                }
            }
            for (int j = 0; j < right_corner.Count; j++)
            {
                if (!type_ele.Contains(right_corner[j]))
                {
                    right_result.Add(right_corner[j]);
                    right_corner.RemoveAt(j);
                    j = 0;
                    if (right_corner.Count == 0)
                        break;                    
                }
            }
        }

        //Thế vào phản ứng. Cặp chất ban đầu thế vào những chỗ chưa biết trong phản ứng:
        //VD: NaCl->NaNO3
        //Phản ứng: MuoiT+MuoiT=MuoiT+MuoiKT
        //Sau khi thế vào: NaCl+MuoiT=MuoiKT+NaNO3
        //Ta chỉ cần phải xét 2 chỗ trống
        public void ReplacetoReaction()
        {
            Element fi = new Element(first_ele);
            Element la = new Element(last_ele);
            String typeof_fi = fi.getElementType();
            String typeof_la = la.getElementType();
            if(left_corner.IndexOf(typeof_fi)!=-1)
            {
                left_corner.RemoveAt(left_corner.IndexOf(typeof_fi));
                left_result.Add(first_ele);
            }
            if (right_corner.IndexOf(typeof_la) != -1)
            {
                right_corner.RemoveAt(right_corner.IndexOf(typeof_la));
                right_result.Add(last_ele);
            }
        }

        //Chọn những chất thỏa điều kiện có trước
        //Ví dụ: NaNO3. Na2SO4, CaSO4, NaCl, KCl, KNO3, Ca(NO3)2
        //Cần tìm chất mà có chứa NO3 trong gốc
        //Hàm này sẽ tìm ra NaNO3, Ca(NO3)2, KNO3
        public List<String> choseElement(List<String>ele_side, List<String>ele_Set)
        {
            if (ele_Set.Count > 1)
            {
                for (int i = 0; i < ele_side.Count; i++)
                {
                    List<String> temp_ele = new List<String>();

                    for (int j = 0; j < ele_Set.Count; j++)
                    {
                        Element ele = new Element(ele_Set[j]);
                        ele.SplitElement();
                        String left_ele = ele.getPosIon();
                        String right_ele = ele.getNegIon();
                        //if (ele_Set[j].IndexOf(ele_side[i]) > -1)
                        if ((left_ele != "" && left_ele.IndexOf(ele_side[i]) > -1) || (right_ele != "" && right_ele.IndexOf(ele_side[i]) > -1))
                        {
                            temp_ele.Add(ele_Set[j]);
                        }
                    }
                    ele_Set = temp_ele;
                }
            }
            return ele_Set;
        }

        //Bỏ đi những gốc trùng nhau ở 2 vế trái và phải
        public void removeConflic(ref List<String> t_left_side, List<String>t_right_side)
        {
            for (int i = 0; i < t_left_side.Count; i++)
            {
                for (int j = 0; j < t_right_side.Count; j++)
                    if (t_left_side[i] == t_right_side[j])
                    {
                        t_left_side.RemoveAt(i);
                        t_right_side.RemoveAt(j);
                        i = 0;
                        j = 0;
                        if (t_left_side.Count == 0 || t_right_side.Count == 0)
                            break;
                        else
                            continue;
                    }
                if (t_left_side.Count == 0)
                    break;
            }
        }

        //Thế vào những chỗ còn trống trong phản ứng bằng những chất cụ thể nhằm hoàn thiện phản ứng
        public void fitReaction()
        {  
            if (left_corner.Count < 2 && right_corner.Count < 2) 
            {
                List<String> left_side = new List<String>();   //Các gốc bên trái
                List<String> right_side = new List<String>();   //Các gốc bên phải
                 //Lấy ra các gốc bên trái có sẵn
                for (int i = 0; i < left_result.Count; i++)
                {
                    Element ele = new Element(left_result[i]);
                    ele.SplitElement();
                    if (ele.getPosIon() != "")
                        left_side.Add(ele.getPosIon());
                    if (ele.getNegIon() != "")
                        left_side.Add(ele.getNegIon());
                }

                //Lấy ra các gốc bên phải có sẵn
                for (int i = 0; i < right_result.Count; i++)
                {
                    Element ele = new Element(right_result[i]);
                    ele.SplitElement();
                    if (ele.getPosIon() != "")
                        right_side.Add(ele.getPosIon());
                    if (ele.getNegIon() != "")
                        right_side.Add(ele.getNegIon());
                }
                //Xóa đi những gốc có mặt ở cả 2 vế
                Conflic con = new Conflic(left_side, right_side);
                con.removeConflic();
                left_side = con.getLeft();
                right_side = con.getRight();

                if (right_corner.Count == 0)
                {
                    List<String> left_ele = new List<String>();
                    left_ele = Element.getElementfromType(left_corner[0]);
                    List<String> temp_left_ele = choseElement(right_side, left_ele);
                    if(temp_left_ele.Count>=1)
                    {
                        left_corner.RemoveAt(0);
                        left_result.Add(temp_left_ele[0]);
                    }
                }
                else if(left_corner.Count==0)
                {
                    List<String> right_ele = new List<String>();
                    right_ele = Element.getElementfromType(right_corner[0]);
                    List<String> temp_right_ele = choseElement(left_side, right_ele);
                    if (temp_right_ele.Count >= 1)
                    {
                        if (left_result.Contains("O2")||left_result.Contains("Cl")||left_result.Contains("Br")||left_result.Contains("I"))
                        {
                            right_corner.RemoveAt(0);
                            right_result.Add(temp_right_ele[1]);
                        }
                        else
                        {
                            right_corner.RemoveAt(0);
                            right_result.Add(temp_right_ele[0]);
                        }
                    }
                }
                else
                {
                    List<String> left_ele = new List<String>();
                    List<String> right_ele = new List<String>();
                    
                    left_ele = Element.getElementfromType(left_corner[0]);
                    left_ele = choseElement(right_side, left_ele);
                    right_ele = Element.getElementfromType(right_corner[0]);
                    for(int i=0;i<left_ele.Count;i++)
                    {
                        List<String> temp_left_side = new List<String>(left_side);
                        List<String> temp_right_side = new List<String>(right_side);
                        List<String> temp_right_ele = new List<String>(right_ele);
                        String temp_ele = left_ele[i];
                        Element ele = new Element(temp_ele);
                        ele.SplitElement();
                        if(ele.getPosIon()!="")
                            temp_left_side.Add(ele.getPosIon());
                        if (ele.getNegIon() != "")
                            temp_left_side.Add(ele.getNegIon());
                        con = new Conflic(temp_left_side, temp_right_side);
                        con.removeConflic();
                        temp_left_side = con.getLeft();
                        temp_right_side = con.getRight();
                        temp_right_ele = choseElement(temp_left_side, right_ele);
                        if (temp_right_ele.Count >= 1)
                        {
                            if(left_corner.Contains("PKN7")||left_corner.Contains("PKN6")||left_result.Contains("O2"))
                            {
                                right_corner.RemoveAt(0);
                                right_result.Add(temp_right_ele[1]);
                                left_corner.RemoveAt(0);
                                left_result.Add(temp_ele);
                            }
                            else
                            {
                                right_corner.RemoveAt(0);
                                right_result.Add(temp_right_ele[0]);
                                left_corner.RemoveAt(0);
                                left_result.Add(temp_ele);
                            }                         
                            break;
                        }
                        temp_left_side.Clear();
                        temp_right_ele.Clear();
                        temp_right_ele.Clear();
                    }
                }

            }
        }

        //Hàm tổng hợp những công việc trên và trả về kết quả là chuỗi hoàn chỉnh
        public String getReaction()
        {
            splitReaction();
            PreProcessing();
            ReplacetoReaction();
            if (left_corner.Count == 0 && right_corner.Count == 0)
            {
                left_result.Reverse();
                right_result.Reverse();
                String t = "";
                for (int i = 0; i < left_result.Count; i++)
                    t = t + left_result[i] + "+";
                t = t.Substring(0, t.Length - 1);
                t = t + "->";
                for (int i = 0; i < right_result.Count; i++)
                    t = t + right_result[i] + "+";
                t = t.Substring(0, t.Length - 1);
                return t;
            }
            else
            {
                fitReaction();
                if (left_corner.Count == 0 && right_corner.Count == 0)
                {
                    left_result.Reverse();
                    right_result.Reverse();
                    String t = "";
                    for (int i = 0; i < left_result.Count; i++)
                        t = t + left_result[i] + "+";
                    t = t.Substring(0, t.Length - 1);
                    t = t + "->";
                    for (int i = 0; i < right_result.Count; i++)
                        t = t + right_result[i] + "+";
                    t = t.Substring(0, t.Length - 1);
                    return t;
                }
                else
                    return "";
            }
        }

    }
}
