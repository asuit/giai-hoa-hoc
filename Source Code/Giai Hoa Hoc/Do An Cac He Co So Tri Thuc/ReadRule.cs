﻿//--------------------------------------------------------------------------------------------------
// Final project - Knowledge base systems
// Solving chain reactions
// Course's ID: CS217.H21.KHTN
// Lecturers: Assoc Prof. Do Van Nhon - Msc. Huynh Thi Thanh Thuong
// Authors: Do Phu An (14520002) - Luu Thanh Son (14520772).
//--------------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Do_An_Cac_He_Co_So_Tri_Thuc
{
    //Xử lý luật
    class ReadRule
    {
        String name;
        //StreamReader read;
        List<Rule> listRule = new List<Rule>();
        public bool isRead = false;

        public ReadRule()
        {
            this.name = @"F:\UIT_Learning\CSTT\Source Code\Giai Hoa Hoc\Do An Cac He Co So Tri Thuc\Rules.txt";
        }

        public ReadRule(String name)
        {
            this.name = name;
        }

        public List<Rule> getListOfRules()
        {
            return this.listRule;
        }

        public void readRule()
        {
            // Read all line in the "Rule.txt" and store it in an array of string.
            // After that, extract all rules into "Rule" and add it to listRule. 

            try
            {
                string[] rules = File.ReadAllLines(this.name, Encoding.UTF8);
                foreach (string substr in rules)
                {
                    Rule r = new Rule();
                    r = splitRule(substr);
                    if (r != null)
                        listRule.Add(r);
                }
                isRead = true;
            }
            catch (Exception e)
            {
                const string message = "Error! Can not read rules from file!";
                const string caption = "Error";
                var result = MessageBox.Show(message, caption,
                                             MessageBoxButtons.OK,
                                             MessageBoxIcon.Error);
            }
        }
        //Tách luật
        public Rule splitRule(String line)
        {
            // Those code lines are used for extracting all elements in a line in rules' list. 
            List<string> temp = new List<string>();
            try
            {
                string pattern = @"[^\s]+";
                Regex regx = new Regex(pattern, RegexOptions.IgnoreCase);
                
                MatchCollection matches = regx.Matches(line);
                foreach (Match match in matches)
                {
                    temp.Add(match.Value);
                }
            }
            catch (Exception e)
            {
                return null;
            }
            return new Rule(temp.ElementAt(0), temp.ElementAt(1), temp.ElementAt(2));
        }

        public List<Rule> getListofRule()
        {
            return this.listRule;
        }
    }
}
