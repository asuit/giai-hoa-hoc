﻿namespace Do_An_Cac_He_Co_So_Tri_Thuc
{
    partial class Search
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.searchTxtBox = new System.Windows.Forms.TextBox();
            this.tabPanel = new System.Windows.Forms.TabControl();
            this.featuresTabPage = new System.Windows.Forms.TabPage();
            this.featuresTxtBox = new System.Windows.Forms.TextBox();
            this.imageTabPage = new System.Windows.Forms.TabPage();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.tabPanel.SuspendLayout();
            this.featuresTabPage.SuspendLayout();
            this.imageTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Search";
            // 
            // searchTxtBox
            // 
            this.searchTxtBox.Location = new System.Drawing.Point(60, 10);
            this.searchTxtBox.Name = "searchTxtBox";
            this.searchTxtBox.Size = new System.Drawing.Size(241, 20);
            this.searchTxtBox.TabIndex = 1;
            this.searchTxtBox.TextChanged += new System.EventHandler(this.searchTxtBox_TextChanged);
            this.searchTxtBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.searchTxtBox_OnKeyDown);
            // 
            // tabPanel
            // 
            this.tabPanel.Controls.Add(this.featuresTabPage);
            this.tabPanel.Controls.Add(this.imageTabPage);
            this.tabPanel.Location = new System.Drawing.Point(16, 41);
            this.tabPanel.Name = "tabPanel";
            this.tabPanel.SelectedIndex = 0;
            this.tabPanel.Size = new System.Drawing.Size(363, 327);
            this.tabPanel.TabIndex = 5;
            // 
            // featuresTabPage
            // 
            this.featuresTabPage.Controls.Add(this.featuresTxtBox);
            this.featuresTabPage.Location = new System.Drawing.Point(4, 22);
            this.featuresTabPage.Name = "featuresTabPage";
            this.featuresTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.featuresTabPage.Size = new System.Drawing.Size(355, 301);
            this.featuresTabPage.TabIndex = 0;
            this.featuresTabPage.Text = "Features";
            this.featuresTabPage.UseVisualStyleBackColor = true;
            // 
            // featuresTxtBox
            // 
            this.featuresTxtBox.Location = new System.Drawing.Point(0, 0);
            this.featuresTxtBox.MaxLength = 64000;
            this.featuresTxtBox.Multiline = true;
            this.featuresTxtBox.Name = "featuresTxtBox";
            this.featuresTxtBox.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.featuresTxtBox.Size = new System.Drawing.Size(355, 301);
            this.featuresTxtBox.TabIndex = 0;
            // 
            // imageTabPage
            // 
            this.imageTabPage.Controls.Add(this.pictureBox);
            this.imageTabPage.Location = new System.Drawing.Point(4, 22);
            this.imageTabPage.Name = "imageTabPage";
            this.imageTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.imageTabPage.Size = new System.Drawing.Size(355, 301);
            this.imageTabPage.TabIndex = 1;
            this.imageTabPage.Text = "Image";
            this.imageTabPage.UseVisualStyleBackColor = true;
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(0, 0);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(355, 301);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            this.pictureBox.WaitOnLoad = true;
            // 
            // Search
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(391, 380);
            this.Controls.Add(this.tabPanel);
            this.Controls.Add(this.searchTxtBox);
            this.Controls.Add(this.label1);
            this.Name = "Search";
            this.Text = "Search";
            this.tabPanel.ResumeLayout(false);
            this.featuresTabPage.ResumeLayout(false);
            this.featuresTabPage.PerformLayout();
            this.imageTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox searchTxtBox;
        private System.Windows.Forms.TabControl tabPanel;
        private System.Windows.Forms.TabPage featuresTabPage;
        private System.Windows.Forms.TabPage imageTabPage;
        private System.Windows.Forms.TextBox featuresTxtBox;
        private System.Windows.Forms.PictureBox pictureBox;
    }
}