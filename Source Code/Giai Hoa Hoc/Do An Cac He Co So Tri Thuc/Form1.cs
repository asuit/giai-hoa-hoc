﻿//--------------------------------------------------------------------------------------------------
// Final project - Knowledge base systems
// Solving chain reactions
// Course's ID: CS217.H21.KHTN
// Lecturers: Assoc Prof. Do Van Nhon - Msc. Huynh Thi Thanh Thuong
// Authors: Do Phu An (14520002) - Luu Thanh Son (14520772).
//--------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Do_An_Cac_He_Co_So_Tri_Thuc
{
    public partial class Form1 : Form
    {
        string chainReaction { get; set; }
        string[] chainReactions { get; set; }
        private Search window;

        List<Rule> rules = new List<Rule>();
        bool isFile = false;
        public Form1()
        {
            InitializeComponent();
            menuBox.Items.Insert(0, "Enter the chain reaction");
            menuBox.Items.Insert(1, "Import file");
            menuBox.Items.Insert(2, "Import rules");

            ReadRule rr = new ReadRule();
            rr.readRule();
            rules = rr.getListofRule();
            if (rr.isRead)
                txtOutput.Text += "Read rule from file... Done!" + Environment.NewLine;
            else txtOutput.Text += "Fail to load all rules from file. \nPlease import another one!" + Environment.NewLine;

            foreach (Rule r in rules)
            {
                txtOutput.Text += r.getFactor() + " " + r.getHypos() + " " + r.getReaction() + Environment.NewLine;
            }
        }

        // Function for handling all events happen on the form. 
        private void Form1_Load(object sender, EventArgs e)
        {
            this.KeyPreview = true;
            this.KeyDown += new KeyEventHandler(Form1_KeyDown);
        }

        // F1 key pressed handling. 
        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                try
                {
                    window = new Search();
                    window.ShowDialog();
                }
                finally
                {
                    if (window != null)
                    {
                        window.Dispose();
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Processing p = new Processing();
            resTxtBox.Text += "Processing..." + Environment.NewLine;
            p.setRule(this.rules);
            p.setInput(txtInput.Text);
            List<String> res = p.getResult();
            for (int i = 0; i < res.Count; i++)
                resTxtBox.Text += res[i] + "\n";
            //Reaction re = new Reaction("PKN6+O2=OXITA", "S", "SO2");
            //String q = re.getReaction();
        }

        private void txtInput_Click(object sender, EventArgs e)
        {
            txtInput.Text = "";
            this.txtInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

        }

        private void txtInput_TextChanged(object sender, EventArgs e)
        {
            if (txtInput.Text == "Import file" || txtInput.Text == "Enter the chain reaction")
            {
                this.txtInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            }
            newBtn.Enabled = txtInput.Text == "" ? false : true;
        }

        // Press enter to start to find result. 
        // Only for enter chain reaction.
        private void txtInput_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtInput.Text != "Enter the chain reaction" && menuBox.SelectedItem.ToString() == "Enter the chain reaction")
                {
                    Processing p = new Processing();
                    resTxtBox.Text += "Processing..." + Environment.NewLine;
                    p.setRule(this.rules);
                    p.setInput(txtInput.Text);
                    List<String> res = p.getResult();
                    for (int i = 0; i < res.Count; i++)
                        resTxtBox.Text += res[i] + Environment.NewLine;
                }
            }
        }

        private void newBtn_Click(object sender, EventArgs e)
        {
            txtInput.Text = "";
            txtOutput.Text = "";
            resTxtBox.Text = "";
        }

        private void menuBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (menuBox.SelectedIndex.ToString())
            {
                case "0": // enter chain reaction
                    txtInput.Text = "Enter the chain reaction";
                    importFileBtn.Enabled = false;
                    isFile = false;
                    this.txtInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    break;
                case "1": // import file
                    txtInput.Text = "Import file";
                    importFileBtn.Enabled = true;
                    isFile = true;
                    this.txtInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    break;
                case "2": // import rules
                    txtInput.Text = "Import rules";
                    importFileBtn.Enabled = true;
                    this.txtInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    break;
                default:
                    this.txtInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    break;
            }
        }

        private void importFileBtn_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.InitialDirectory = @"C:\";
            openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog.FilterIndex = 2;
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                // Import file  
                string fileName = openFileDialog.FileName;
                chainReactions = File.ReadAllLines(fileName, Encoding.UTF8);

                txtInput.Text = openFileDialog.FileName;
                this.txtInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                if (menuBox.SelectedItem.ToString() == "Import file")
                {
                    foreach (string line in chainReactions)
                    {
                        Processing p = new Processing();
                        resTxtBox.Text += "Processing..." + Environment.NewLine;
                        p.setRule(this.rules);
                        p.setInput(line);
                        List<String> res = p.getResult();
                        for (int i = 0; i < res.Count; i++)
                            resTxtBox.Text += res[i] + Environment.NewLine;
                        resTxtBox.Text += Environment.NewLine;
                    }
                }
                else if (menuBox.SelectedItem.ToString() == "Import rules")
                {
                    ReadRule rr = new ReadRule(fileName);
                    rr.readRule();
                    rules = rr.getListofRule();

                    txtOutput.Text += "Read rule from file... Done!" + Environment.NewLine;
                    foreach (Rule r in rules)
                    {
                        txtOutput.Text += r.getFactor() + " " + r.getHypos() + " " + r.getReaction() + Environment.NewLine;
                    }
                }
            }

        }

        // Save result to text file given by the user. 
        private void saveBtn_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();

            saveFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog.FilterIndex = 2;
            saveFileDialog.RestoreDirectory = true;

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                // save all results to file
                List<string> lines = new List<string>();
                foreach (string line in resTxtBox.Lines)
                {
                    lines.Add(line);
                }
                string[] linesArray = lines.ToArray();
                File.WriteAllLines(saveFileDialog.FileName, linesArray);
            }
        }

        private void resTxtBox_TextChanged(object sender, EventArgs e)
        {
            saveBtn.Enabled = resTxtBox.Text == "" ? false : true;
            newBtn.Enabled = (resTxtBox.Text != "" ? true : false);
        }
    }
}
