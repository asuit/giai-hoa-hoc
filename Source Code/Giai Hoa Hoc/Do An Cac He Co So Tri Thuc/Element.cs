﻿//--------------------------------------------------------------------------------------------------
// Final project - Knowledge base systems
// Solving chain reactions
// Course's ID: CS217.H21.KHTN
// Lecturers: Assoc Prof. Do Van Nhon - Msc. Huynh Thi Thanh Thuong
// Authors: Do Phu An (14520002) - Luu Thanh Son (14520772).
//--------------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Do_An_Cac_He_Co_So_Tri_Thuc
{
    //Lớp này xử lý trên Chất hóa học
    class Element
    {
        String Chat;   //Ký hiệu chất hóa học
        String PosIon;   //Gốc dương
        String NegIon;    //Gốc âm
        public Element(String chat)
        {
            this.Chat = chat;
        }
        public Element()
        {

        }
        //Tra cứu cơ sở tri thức (lưu bằng sql) để lấy ra loại của chất
        public String getElementType()
        {
            String sql = "select Loai from Chat where KyHieu=" + "\'" + Chat + "\'";
            String result = "";

            SqlConnection conn = DatabaseConnect.GetDBConnection(ConnectString.getConnString());

            try
            {
                conn.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = conn;
                command.CommandText = sql;
                result = command.ExecuteScalar().ToString();
            }
            catch(Exception e)
            {
                
            }
            finally
            {
                conn.Close();
            }
            return result;
        }

        //Tra cứu xem Chất thuộc loại gì ?
        public static List<String>getElementfromType(String type)
        {
            String sql = "select KyHieu from Chat where Loai=" + "\'" + type + "\'";
            List<String> ele = new List<String>();

            SqlConnection conn = DatabaseConnect.GetDBConnection(ConnectString.getConnString());
            try
            {
                conn.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = conn;
                command.CommandText = sql;
                SqlDataReader reader = command.ExecuteReader();
                while(reader.Read())
                {
                    ele.Add(reader[0].ToString());
                }
            }
            catch (Exception e)
            {

            }
            finally
            {
                conn.Close();
            }
            return ele;
        }
        //Tách chất ra làm 2 gốc
        public void SplitElement()
        {
            String t = "";
            int q = 0;
            if (Chat == "H2O")
            {
                PosIon = "H";
                NegIon = "OH";
            }
            else
                if(getElementType()=="OXITA")
                {
                    for (int i = 0; i < Chat.Length; i++)
                        if (Chat[i] >= 'A' && Chat[i] <= 'Z' || Chat[i] >= 'a' && Chat[i] <= 'z')
                            t = t + Chat[i];
                    PosIon = "";
                    NegIon = t;
                }
                else
                {
                if (Chat.Length < 2)
                {
                    t = t + Chat[0];
                    PosIon = t;
                    NegIon = "";
                }
                else
                {
                        t = t + Chat[0];
                        if (Chat[1] >= 'a' && Chat[1] <= 'z')
                        {
                            t = t + Chat[1];
                            this.PosIon = t;
                            t = "";
                            for (int i = 2; i < Chat.Length; i++)
                                if (Chat[i] >= 'a' && Chat[i] < 'z' || Chat[i] >= 'A' && Chat[i] <= 'Z')
                                {
                                    t = t + Chat[i];
                                    q = i;
                                }
                        }
                        else
                        {
                            this.PosIon = t;
                            t = "";
                            for (int i = 1; i < Chat.Length; i++)
                                if (Chat[i] >= 'a' && Chat[i] < 'z' || Chat[i] >= 'A' && Chat[i] <= 'Z')
                                {
                                    t = t + Chat[i];
                                    q = i;
                                }
                        }
                            this.NegIon = t;
                   }
                }
        }
        public String getPosIon()
        {
            return this.PosIon;
        }
        public String getNegIon()
        {
            return this.NegIon;
        }

        //Lấy ra toàn bộ các loại chất trong CSTT
        public static List<String> getAllElementType()
        {
            String sql = "select Loai from Loai";
            List<String> type_ele = new List<String>();

            SqlConnection conn = DatabaseConnect.GetDBConnection(ConnectString.getConnString());
            try
            {
                conn.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = conn;
                command.CommandText = sql;
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    type_ele.Add(reader[0].ToString());
                }
            }
            catch (Exception e)
            {

            }
            finally
            {
                conn.Close();
            }
            return type_ele;
        }
    }
}
