﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Do_An_Cac_He_Co_So_Tri_Thuc
{
    //Kết nối Cơ sở dữ liệu
    class DatabaseConnect
    {
        public DatabaseConnect()
        {

        }
        public static SqlConnection GetDBConnection(string conString)
        {
            SqlConnection conn = new SqlConnection(conString);

            return conn;
        }
      
    }
}
