﻿//--------------------------------------------------------------------------------------------------
// Final project - Knowledge base systems
// Solving chain reactions
// Course's ID: CS217.H21.KHTN
// Lecturers: Assoc Prof. Do Van Nhon - Msc. Huynh Thi Thanh Thuong
// Authors: Do Phu An (14520002) - Luu Thanh Son (14520772).
//--------------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Do_An_Cac_He_Co_So_Tri_Thuc
{
    //Lớp này định nghĩa cấu trúc của một luật. gồm 3 phần:
    //hypos: chất đầu
    //factor: chất cuối
    //reaction: phản ứng giữa chất đầu và chất cuối
    class Rule
    {
        String hypos;
        String factor;
        String reaction;
      
        
        public Rule()
        {
            hypos = factor = reaction = "";
        }

        // new constructor for storing Rules. 
        public Rule(String v1, String v2, String v3)
        {
            this.setHypos(v1);
            this.setFactor(v2);
            this.setReaction(v3);
        }

        public void setHypos(String hypos) { this.hypos = hypos; }

        public void setFactor(String factor) { this.factor = factor; }

        public void setReaction(String reaction){ this.reaction = reaction; }

        public String getHypos() { return this.hypos;}

        public String getFactor() { return this.factor;}

        public String getReaction() { return this.reaction; }
    }
}
