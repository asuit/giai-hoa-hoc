﻿//--------------------------------------------------------------------------------------------------
// Final project - Knowledge base systems
// Solving chain reactions
// Course's ID: CS217.H21.KHTN
// Lecturers: Assoc Prof. Do Van Nhon - Msc. Huynh Thi Thanh Thuong
// Authors: Do Phu An (14520002) - Luu Thanh Son (14520772).
//--------------------------------------------------------------------------------------------------
namespace Do_An_Cac_He_Co_So_Tri_Thuc
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtInput = new System.Windows.Forms.TextBox();
            this.txtOutput = new System.Windows.Forms.RichTextBox();
            this.generateBtn = new System.Windows.Forms.Button();
            this.resTxtBox = new System.Windows.Forms.RichTextBox();
            this.menuBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.newBtn = new System.Windows.Forms.Button();
            this.saveBtn = new System.Windows.Forms.Button();
            this.importFileBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtInput
            // 
            this.txtInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInput.Location = new System.Drawing.Point(12, 12);
            this.txtInput.Name = "txtInput";
            this.txtInput.Size = new System.Drawing.Size(420, 20);
            this.txtInput.TabIndex = 0;
            this.txtInput.Click += new System.EventHandler(this.txtInput_Click);
            this.txtInput.TextChanged += new System.EventHandler(this.txtInput_TextChanged);
            this.txtInput.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtInput_KeyDown);
            // 
            // txtOutput
            // 
            this.txtOutput.Location = new System.Drawing.Point(12, 65);
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.ReadOnly = true;
            this.txtOutput.Size = new System.Drawing.Size(208, 309);
            this.txtOutput.TabIndex = 2;
            this.txtOutput.Text = "";
            // 
            // generateBtn
            // 
            this.generateBtn.Location = new System.Drawing.Point(239, 389);
            this.generateBtn.Name = "generateBtn";
            this.generateBtn.Size = new System.Drawing.Size(75, 26);
            this.generateBtn.TabIndex = 3;
            this.generateBtn.Text = "Solve";
            this.generateBtn.UseVisualStyleBackColor = true;
            this.generateBtn.Click += new System.EventHandler(this.button1_Click);
            // 
            // resTxtBox
            // 
            this.resTxtBox.Location = new System.Drawing.Point(239, 65);
            this.resTxtBox.Name = "resTxtBox";
            this.resTxtBox.ReadOnly = true;
            this.resTxtBox.Size = new System.Drawing.Size(328, 309);
            this.resTxtBox.TabIndex = 5;
            this.resTxtBox.Text = "";
            this.resTxtBox.TextChanged += new System.EventHandler(this.resTxtBox_TextChanged);
            // 
            // menuBox
            // 
            this.menuBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.menuBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.menuBox.FormattingEnabled = true;
            this.menuBox.Location = new System.Drawing.Point(446, 11);
            this.menuBox.Name = "menuBox";
            this.menuBox.Size = new System.Drawing.Size(121, 21);
            this.menuBox.TabIndex = 7;
            this.menuBox.SelectedIndexChanged += new System.EventHandler(this.menuBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Rules";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(239, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Results";
            // 
            // newBtn
            // 
            this.newBtn.Enabled = false;
            this.newBtn.Location = new System.Drawing.Point(321, 389);
            this.newBtn.Name = "newBtn";
            this.newBtn.Size = new System.Drawing.Size(75, 26);
            this.newBtn.TabIndex = 10;
            this.newBtn.Text = "New";
            this.newBtn.UseVisualStyleBackColor = true;
            this.newBtn.Click += new System.EventHandler(this.newBtn_Click);
            // 
            // saveBtn
            // 
            this.saveBtn.Enabled = false;
            this.saveBtn.Location = new System.Drawing.Point(402, 389);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(75, 26);
            this.saveBtn.TabIndex = 11;
            this.saveBtn.Text = "Save";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // importFileBtn
            // 
            this.importFileBtn.Enabled = false;
            this.importFileBtn.Location = new System.Drawing.Point(12, 389);
            this.importFileBtn.Name = "importFileBtn";
            this.importFileBtn.Size = new System.Drawing.Size(75, 26);
            this.importFileBtn.TabIndex = 12;
            this.importFileBtn.Text = "Import File";
            this.importFileBtn.UseVisualStyleBackColor = true;
            this.importFileBtn.Click += new System.EventHandler(this.importFileBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(578, 432);
            this.Controls.Add(this.importFileBtn);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.newBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuBox);
            this.Controls.Add(this.resTxtBox);
            this.Controls.Add(this.generateBtn);
            this.Controls.Add(this.txtOutput);
            this.Controls.Add(this.txtInput);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtInput;
        private System.Windows.Forms.RichTextBox txtOutput;
        private System.Windows.Forms.Button generateBtn;
        private System.Windows.Forms.RichTextBox resTxtBox;
        private System.Windows.Forms.ComboBox menuBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button newBtn;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Button importFileBtn;
    }
}

