﻿create database HoaHoc
USE HoaHoc
USE master
drop database HoaHoc
create table Chat
(
	KyHieu varchar(50) not null,
	Loai varchar(10),
)
alter table Chat add constraint pk_chat primary key(KyHieu)
alter table Chat add constraint fk_loai foreign key(Loai) references Loai(Loai)
create table Loai
(
	Loai varchar(10) not null,
	TenLoai varchar(50)
)
alter table Loai add constraint pk_loai primary key(Loai)

create table ThongTin
(
	KyHieu varchar(50),
	TenGoi varchar(128),
	DacDiem Nchar(2048),
	HoaTri varchar(50),
	NguyenTuKhoi float,
	PicturePath varchar(256)
)
alter table ThongTin add constraint fk_chat foreign key (KyHieu) references Chat(KyHieu)


/* Một vài đơn chất*/
/* KL kiềm*/
insert into ThongTin (KyHieu, TenGoi, DacDiem,HoaTri,NguyenTuKhoi,PicturePath) values ('Li', 'Lithium',N'Lithium is a soft, silvery-white alkali metal. Under standard conditions, it is the lightest metal and the lightest solid element. Like all alkali metals, lithium is highly reactive and flammable, and is stored in mineral oil.', '1', 7, 'Li.JPG')
insert into ThongTin (KyHieu, TenGoi, DacDiem,HoaTri,NguyenTuKhoi,PicturePath) values ('Na', 'Sodium',N'Sodium is a soft, silvery-white, highly reactive metal. Sodium is an alkali metal, being in group 1 of the periodic table, because it has a single electron in its outer shell that it readily donates, creating a positively charged atom—the Na+ cation. Its only stable isotope is 23Na', '1', 23, 'Na.jpg')
insert into ThongTin (KyHieu, TenGoi, DacDiem,HoaTri,NguyenTuKhoi,PicturePath) values ('K', 'Potassium',N'Potassium was first isolated from potash, the ashes of plants, from which its name derives. In the periodic table, potassium is one of the alkali metals.', '1', 39, 'K.jpg')

/* KL kiềm thổ*/
insert into ThongTin (KyHieu, TenGoi, DacDiem,HoaTri,NguyenTuKhoi,PicturePath) values ('Ca', 'Calcium',N'An alkaline earth metal, calcium is a reactive pale yellow metal that forms a dark oxide-nitride layer when exposed to air. Its physical and chemical properties are most similar to its heavier homologues strontium and barium. It is the fifth most abundant element in Earths crust and the third most abundant metal, after iron and aluminium.', '2', 40, 'Ca.jpg')
insert into ThongTin (KyHieu, TenGoi, DacDiem,HoaTri,NguyenTuKhoi,PicturePath) values ('Ba', 'Barium',N'is the fifth element in group 2 and is a soft, silvery alkaline earth metal. Because of its high chemical reactivity, barium is never found in nature as a free element.', '2', 137, 'Ba.jpg')

/* Kim loai khac*/
insert into ThongTin (KyHieu, TenGoi, DacDiem,HoaTri,NguyenTuKhoi,PicturePath) values ('Al', 'Aluminium',N'Aluminium is a silvery-white, soft, nonmagnetic, ductile metal', '3', 27, 'F:\UIT_Learning\CSTT\Database\Al.jpg')
insert into ThongTin (KyHieu, TenGoi, DacDiem,HoaTri,NguyenTuKhoi,PicturePath) values ('Cr', 'Chromium',N'Chromium is the first element in group 6. It is a steely-grey, lustrous, hard and brittle metal[4] which takes a high polish, resists tarnishing, and has a high melting point.', '6, 5, 4, 3, 2, 1, −1, −2, −4 ', 52, 'F:\UIT_Learning\CSTT\Database\Cr.jpg')
insert into ThongTin (KyHieu, TenGoi, DacDiem,HoaTri,NguyenTuKhoi,PicturePath) values ('Zn', 'Zinc',N'Zinc is the first element in group 12 of the periodic table. In some respects zinc is chemically similar to magnesium: both elements exhibit only one normal oxidation state (+2), and the Zn2+ and Mg2+ ions are of similar size. Zinc is the 24th most abundant element in Earths crust and has five stable isotopes','-2, 0, +1, +2', 65, 'Zn.jpg')

/*Phi kim*/
insert into ThongTin (KyHieu, TenGoi, DacDiem,HoaTri,NguyenTuKhoi,PicturePath) values ('C', 'Carbon',N'Carbon is nonmetallic and tetravalent—making four electrons available to form covalent chemical bonds. Three isotopes occur naturally, 12C and 13C being stable, while 14C is a radioactive isotope, decaying with a half-life of about 5,730 years. Carbon is one of the few elements known since antiquity.', '+4, +3, +2, +1, 0, −1, −2, −3, −4', 12, 'C.jpg')
insert into ThongTin (KyHieu, TenGoi, DacDiem,HoaTri,NguyenTuKhoi,PicturePath) values ('Si', 'Silicon',N'Silicon is a chemical element with symbol Si and atomic number 14. A hard and brittle crystalline solid with a blue-gray metallic luster, it is a tetravalent metalloid. It is a member of group 14 in the periodic table, along with carbon above it and germanium, tin, lead, and flerovium below. It is not very reactive, although more reactive than carbon, and has great chemical affinity for oxygen; it was first purified and characterized in 1823 by Jöns Jakob Berzelius.', '4, 3, 2, 1, −1, −2, −3, −4', 28, 'Si.jpg')
insert into ThongTin (KyHieu, TenGoi, DacDiem,HoaTri,NguyenTuKhoi,PicturePath) values ('P', 'Phosphorus',N'phosphorus exists in two major forms—white phosphorus and red phosphorus—but because it is highly reactive, phosphorus is never found as a free element on Earth.', '5, 4, 3, 2, 1, −1, −2, −3 ​', 31, 'P.jpg')

/*Phi kim halogen*/
insert into ThongTin (KyHieu, TenGoi, DacDiem,HoaTri,NguyenTuKhoi,PicturePath) values ('Cl', 'Chlorine',N'The second-lightest of the halogens, it appears between fluorine and bromine in the periodic table and its properties are mostly intermediate between them. Chlorine is a yellow-green gas at room temperature. It is an extremely reactive element and a strong oxidising agent: among the elements, it has the highest electron affinity and the third-highest electronegativity, behind only oxygen and fluorine.', '7, 6, 5, 4, 3, 2, 1, −1​', 35.5, 'Cl.jpg')
insert into ThongTin (KyHieu, TenGoi, DacDiem,HoaTri,NguyenTuKhoi,PicturePath) values ('Br', 'Bromine',N'It is the third-lightest halogen, and is a fuming red-brown liquid at room temperature that evaporates readily to form a similarly coloured gas. Its properties are thus intermediate between those of chlorine and iodine.', '7, 5, 4, 3, 1, −1​', 80, 'Br.jpg')

/* Một vài hợp chất*/
insert into ThongTin (KyHieu, TenGoi, DacDiem,HoaTri,NguyenTuKhoi,PicturePath) values ('NaOH', 'Sodium Hydroxide',N'Sodium Hydroxide is a white solid ionic compound consisting of sodium cations Na+ and hydroxide anions OH−. Sodium hydroxide is a highly caustic base and alkali, that decomposes proteins at ordinary ambient temperatures and may cause severe chemical burns.', 'None', 40, 'NaOH.jpg')
insert into ThongTin (KyHieu, TenGoi, DacDiem,HoaTri,NguyenTuKhoi,PicturePath) values ('Ca(OH)2', 'Calcium Hydroxide',N'Calcium hydroxide (traditionally called slaked lime) is an inorganic compound with the chemical formula Ca(OH)2. It is a colorless crystal or white powder and is obtained when calcium oxide.', 'None', 74, 'Ca(OH)2.jpg')

/* Bazo không tan*/
insert into ThongTin (KyHieu, TenGoi, DacDiem,HoaTri,NguyenTuKhoi,PicturePath) values ('Al(OH)3', 'Aluminium Hydroxide',N'Aluminium hydroxide, Al(OH)3, is found in nature as the mineral gibbsite (also known as hydrargillite) and its three much rarer polymorphs: bayerite, doyleite, and nordstrandite. Aluminium hydroxide is amphoteric in nature, i.e, it has both basic and acidic properties.', 'None', 73, 'Al(OH)3.jpg')
insert into ThongTin (KyHieu, TenGoi, DacDiem,HoaTri,NguyenTuKhoi,PicturePath) values ('Fe(OH)2', 'Ferrous Hydroxide',N'Iron(II) hydroxide or ferrous hydroxide is an inorganic compound with the formula Fe(OH)2. It is produced when iron(II) salts, from a compound such as iron(II) sulfate, are treated with hydroxide ions. Iron(II) hydroxide is a white solid, but even traces of oxygen impart a greenish tinge. The air-oxidized solid is sometimes known as "green rust"', 'None', 90, 'Fe(OH)2.jpg')

/* Axit mạnh*/
insert into ThongTin (KyHieu, TenGoi, DacDiem,HoaTri,NguyenTuKhoi,PicturePath) values ('HCl', 'Hydrogen Chloride',N'The compound hydrogen chloride has the chemical formula HCl and as such is a hydrogen halide. At room temperature, it is a colorless gas, which forms white fumes of hydrochloric acid upon contact with atmospheric water vapor.', 'None', 36.5, 'HCl.jpg')
insert into ThongTin (KyHieu, TenGoi, DacDiem,HoaTri,NguyenTuKhoi,PicturePath) values ('H2SO4', 'Sulfuric Acid',N'Sulfuric acid (alternative spelling sulphuric acid) is a highly corrosive strong mineral acid with the molecular formula H2SO4 and molecular weight 98.079 g/mol. It is a pungent-ethereal, colorless to slightly yellow viscous liquid that is soluble in water at all concentrations.', 'None', 98, 'H2SO4.jpg')
insert into ThongTin (KyHieu, TenGoi, DacDiem,HoaTri,NguyenTuKhoi,PicturePath) values ('HNO3', 'Nitric Acid',N'Nitric acid (HNO3), also known as aqua fortis and spirit of niter, is a highly corrosive mineral acid. The pure compound is colorless, but older samples tend to acquire a yellow cast due to decomposition into oxides of nitrogen and water. Most commercially available nitric acid has a concentration of 68% in water. When the solution contains more than 86% HNO3, it is referred to as fuming nitric acid.', 'None', 63, 'HNO3.jpg')


/*Cac loai Chat*/
insert into Loai (Loai,TenLoai) values ('KLK','Kim loai Kiem')
insert into Loai (Loai,TenLoai) values ('KLKT','Kim loai Kiem Tho')
insert into Loai (Loai,TenLoai) values ('KLM','Kim loai Manh')
insert into Loai (Loai,TenLoai) values ('KLLT','Kim loai Luong Tinh')
insert into Loai (Loai,TenLoai) values ('KLTY','Kim loai Yeu')
insert into Loai (Loai,TenLoai) values ('KLQ','Kim loai Quy')
insert into Loai (Loai,TenLoai) values ('PKN4','Phi Kim nhom 4')
insert into Loai (Loai,TenLoai) values ('PKN5','Phi Kim nhom 5')
insert into Loai (Loai,TenLoai) values ('PKN6','Phi Kim nhom 6')
insert into Loai (Loai,TenLoai) values ('PKN7','Phi Kim nhom 7')
insert into Loai (Loai,TenLoai) values ('PKN8','Phi Kim nhom 8')
insert into Loai (Loai,TenLoai) values ('BAZT','Bazo tan')
insert into Loai (Loai,TenLoai) values ('BAZKT','Bazo khong tan')
insert into Loai (Loai,TenLoai) values ('AXITM','Axit manh')
insert into Loai (Loai,TenLoai) values ('AXITY','Axit yeu')
insert into Loai (Loai,TenLoai) values ('AXITOX','Axit oxi hoa')
insert into Loai (Loai,TenLoai) values ('OXITB','Oxit Bazo')
insert into Loai (Loai,TenLoai) values ('OXITA','Oxit Axit')
insert into Loai (Loai,TenLoai) values ('OxitKL','Oxit Kim Loai')
insert into Loai (Loai,TenLoai) values ('OXITTT','Oxit trung tinh')
insert into Loai (Loai,TenLoai) values ('MuoiT','Muoi Tan')
insert into Loai (Loai,TenLoai) values ('MuoiKT','Muoi khong Tan')


select * from Loai

/*Kim loai Kiem*/
insert into Chat (KyHieu,Loai) values ('Li','KLK')
insert into Chat (KyHieu,Loai) values ('Na','KLK')
insert into Chat (KyHieu,Loai) values ('K','KLK')

/*Kim loai Kiem Tho*/
insert into Chat (KyHieu,Loai) values ('Ca','KLKT')
insert into Chat (KyHieu,Loai) values ('Ba','KLKT')

/*Kim loai Luong tinh*/
insert into Chat (KyHieu,Loai) values ('Al','KLLT')
insert into Chat (KyHieu,Loai) values ('Cr','KLLT')
insert into Chat (KyHieu,Loai) values ('Zn','KLLT')

/*Kim loai Manh*/
insert into Chat (KyHieu,Loai) values ('Mg','KLM')
insert into Chat (KyHieu,Loai) values ('Fe','KLM')
insert into Chat (KyHieu,Loai) values ('Mn','KLM')

/*Kim loai Yeu*/
insert into Chat (KyHieu,Loai) values ('Cu','KLTY')
insert into Chat (KyHieu,Loai) values ('Ag','KLTY')
insert into Chat (KyHieu,Loai) values ('Hg','KLTY')

/*Kim loai Quy*/
insert into Chat (KyHieu,Loai) values ('Pt','KLQ')
insert into Chat (KyHieu,Loai) values ('Au','KLQ')

/*Bazo Tan*/
insert into Chat (KyHieu,Loai) values ('NaOH','BAZT')
insert into Chat (KyHieu,Loai) values ('KOH','BAZT')
insert into Chat (KyHieu,Loai) values ('LiOH','BAZT')
insert into Chat (KyHieu,Loai) values ('Ca(OH)2','BAZT')
insert into Chat (KyHieu,Loai) values ('Ba(OH)2','BAZT')

/*Bazo Khong tan*/
insert into Chat (KyHieu,Loai) values ('Al(OH)3','BAZKT')
insert into Chat (KyHieu,Loai) values ('Cr(OH)3','BAZKT')
insert into Chat (KyHieu,Loai) values ('Zn(OH)2','BAZKT')
insert into Chat (KyHieu,Loai) values ('Mg(OH)2','BAZKT')
insert into Chat (KyHieu,Loai) values ('Fe(OH)2','BAZKT')
insert into Chat (KyHieu,Loai) values ('Fe(OH)3','BAZKT')
insert into Chat (KyHieu,Loai) values ('Mn(OH)2','BAZKT')
insert into Chat (KyHieu,Loai) values ('Cu(OH)2','BAZKT')
insert into Chat (KyHieu,Loai) values ('Si(OH)2','BAZKT')

/*Phi kim Nhom 4*/
insert into Chat (KyHieu,Loai) values ('Si','PKN4')
insert into Chat (KyHieu,Loai) values ('C','PKN4')

/*Phi kim nhom 5*/
insert into Chat (KyHieu,Loai) values ('N','PKN5')
insert into Chat (KyHieu,Loai) values ('P','PKN5')

/*Phi kim nhom 6*/
insert into Chat (KyHieu,Loai) values ('S','PKN6')
insert into Chat (KyHieu,Loai) values ('O','PKN6')

/*Phi kim nhom 7*/
insert into Chat (KyHieu,Loai) values ('Cl','PKN7')
insert into Chat (KyHieu,Loai) values ('Br','PKN7')
insert into Chat (KyHieu,Loai) values ('F','PKN7')
insert into Chat (KyHieu,Loai) values ('I','PKN7')

/*Oxit Bazo*/
insert into Chat (KyHieu,Loai) values ('Na2O','OXITB')
insert into Chat (KyHieu,Loai) values ('K2O','OXITB')
insert into Chat (KyHieu,Loai) values ('Li2O','OXITB')
insert into Chat (KyHieu,Loai) values ('CaO','OXITB')
insert into Chat (KyHieu,Loai) values ('BaO','OXITB')

/*Oxit Axit*/
insert into Chat (KyHieu,Loai) values ('SiO2','OXITA')
insert into Chat (KyHieu,Loai) values ('CO2','OXITA')
insert into Chat (KyHieu,Loai) values ('NO2','OXITA')
insert into Chat (KyHieu,Loai) values ('N2O','OXITA')
insert into Chat (KyHieu,Loai) values ('P2O5','OXITA')
insert into Chat (KyHieu,Loai) values ('SO2','OXITA')
insert into Chat (KyHieu,Loai) values ('SO3','OXITA')

/*Oxit Kim loai*/
insert into Chat (KyHieu,Loai) values ('Al2O3','OxitKL')
insert into Chat (KyHieu,Loai) values ('Cr2O3','OxitKL')
insert into Chat (KyHieu,Loai) values ('ZnO','OxitKL')
insert into Chat (KyHieu,Loai) values ('MgO','OxitKL')
insert into Chat (KyHieu,Loai) values ('FeO','OxitKL')
insert into Chat (KyHieu,Loai) values ('Fe2O3','OxitKL')
insert into Chat (KyHieu,Loai) values ('Fe3O4','OxitKL')
insert into Chat (KyHieu,Loai) values ('MnO','OxitKL')
insert into Chat (KyHieu,Loai) values ('CuO','OxitKL')
insert into Chat (KyHieu,Loai) values ('Ag2O','OxitKL')
insert into Chat (KyHieu,Loai) values ('HgO','OxitKL')

/*Oxit trung tinh */
insert into Chat (KyHieu,Loai) values ('CO','OXITTT')
insert into Chat (KyHieu,Loai) values ('NO','OXITTT')

/*Axit manh */
insert into Chat (KyHieu,Loai) values ('HCl','AXITM')
insert into Chat (KyHieu,Loai) values ('HBr','AXITM')
insert into Chat (KyHieu,Loai) values ('HI','AXITM')
insert into Chat (KyHieu,Loai) values ('HF','AXITM')
insert into Chat (KyHieu,Loai) values ('H2SO4','AXITM')
insert into Chat (KyHieu,Loai) values ('H3PO4','AXITM')

/*Axit Yeu*/
insert into Chat (KyHieu,Loai) values ('H2SO3','AXITY')
insert into Chat (KyHieu,Loai) values ('H2CO3','AXITY')

/*Axit Oxi hoa */
insert into Chat (KyHieu,Loai) values ('HNO3','AXITOX')

/*Muoi tan */
insert into Chat (KyHieu,Loai) values ('NaCl','MuoiT')
insert into Chat (KyHieu,Loai) values ('KCl','MuoiT')
insert into Chat (KyHieu,Loai) values ('LiCl','MuoiT')
insert into Chat (KyHieu,Loai) values ('CaCl2','MuoiT')
insert into Chat (KyHieu,Loai) values ('BaCl2','MuoiT')
insert into Chat (KyHieu,Loai) values ('AlCl3','MuoiT')
insert into Chat (KyHieu,Loai) values ('CrCl3','MuoiT')
insert into Chat (KyHieu,Loai) values ('ZnCl2','MuoiT')
insert into Chat (KyHieu,Loai) values ('FeCl2','MuoiT')
insert into Chat (KyHieu,Loai) values ('FeCl3','MuoiT')
insert into Chat (KyHieu,Loai) values ('MgCl2','MuoiT')
insert into Chat (KyHieu,Loai) values ('MnCl2','MuoiT')
insert into Chat (KyHieu,Loai) values ('CuCl2','MuoiT')
insert into Chat (KyHieu,Loai) values ('HgCl2','MuoiT')

insert into Chat (KyHieu,Loai) values ('NaBr','MuoiT')
insert into Chat (KyHieu,Loai) values ('KBr','MuoiT')
insert into Chat (KyHieu,Loai) values ('LiBr','MuoiT')
insert into Chat (KyHieu,Loai) values ('CaBr2','MuoiT')
insert into Chat (KyHieu,Loai) values ('BaBr2','MuoiT')
insert into Chat (KyHieu,Loai) values ('AlBr3','MuoiT')
insert into Chat (KyHieu,Loai) values ('CrBr3','MuoiT')
insert into Chat (KyHieu,Loai) values ('ZnBr2','MuoiT')
insert into Chat (KyHieu,Loai) values ('FeBr2','MuoiT')
insert into Chat (KyHieu,Loai) values ('FeBr3','MuoiT')
insert into Chat (KyHieu,Loai) values ('MgBr2','MuoiT')
insert into Chat (KyHieu,Loai) values ('MnBr2','MuoiT')
insert into Chat (KyHieu,Loai) values ('CuBr2','MuoiT')
insert into Chat (KyHieu,Loai) values ('HgBr2','MuoiT')

insert into Chat (KyHieu,Loai) values ('NaI','MuoiT')
insert into Chat (KyHieu,Loai) values ('KI','MuoiT')
insert into Chat (KyHieu,Loai) values ('LiI','MuoiT')
insert into Chat (KyHieu,Loai) values ('CaI2','MuoiT')
insert into Chat (KyHieu,Loai) values ('BaI2','MuoiT')
insert into Chat (KyHieu,Loai) values ('AlI3','MuoiT')
insert into Chat (KyHieu,Loai) values ('CrI3','MuoiT')
insert into Chat (KyHieu,Loai) values ('ZnI2','MuoiT')
insert into Chat (KyHieu,Loai) values ('FeI2','MuoiT')
insert into Chat (KyHieu,Loai) values ('MgI2','MuoiT')

insert into Chat (KyHieu,Loai) values ('NaNO3','MuoiT')
insert into Chat (KyHieu,Loai) values ('KNO3','MuoiT')
insert into Chat (KyHieu,Loai) values ('LiNO3','MuoiT')
insert into Chat (KyHieu,Loai) values ('Ca(NO3)2','MuoiT')
insert into Chat (KyHieu,Loai) values ('Ba(NO3)2','MuoiT')
insert into Chat (KyHieu,Loai) values ('Al(NO3)3','MuoiT')
insert into Chat (KyHieu,Loai) values ('Cr(NO3)3','MuoiT')
insert into Chat (KyHieu,Loai) values ('Zn(NO3)2','MuoiT')
insert into Chat (KyHieu,Loai) values ('Fe(NO3)2','MuoiT')
insert into Chat (KyHieu,Loai) values ('Fe(NO3)3','MuoiT')
insert into Chat (KyHieu,Loai) values ('Mg(NO3)2','MuoiT')
insert into Chat (KyHieu,Loai) values ('Mn(NO3)2','MuoiT')
insert into Chat (KyHieu,Loai) values ('Cu(NO3)2','MuoiT')
insert into Chat (KyHieu,Loai) values ('AgNO3','MuoiT')
insert into Chat (KyHieu,Loai) values ('Hg(NO3)2','MuoiT')

insert into Chat (KyHieu,Loai) values ('Na2SO4','MuoiT')
insert into Chat (KyHieu,Loai) values ('K2SO4','MuoiT')
insert into Chat (KyHieu,Loai) values ('Li2SO4','MuoiT')
insert into Chat (KyHieu,Loai) values ('Al2(SO4)3','MuoiT')
insert into Chat (KyHieu,Loai) values ('Cr2(SO4)3','MuoiT')
insert into Chat (KyHieu,Loai) values ('ZnSO4','MuoiT')
insert into Chat (KyHieu,Loai) values ('FeSO4','MuoiT')
insert into Chat (KyHieu,Loai) values ('Fe2(SO4)3','MuoiT')
insert into Chat (KyHieu,Loai) values ('MgSO4','MuoiT')
insert into Chat (KyHieu,Loai) values ('MnSO4','MuoiT')
insert into Chat (KyHieu,Loai) values ('CuSO4','MuoiT')

insert into Chat (KyHieu,Loai) values ('Na2CO3','MuoiT')
insert into Chat (KyHieu,Loai) values ('K2CO3','MuoiT')
insert into Chat (KyHieu,Loai) values ('Li2CO3','MuoiT')

insert into Chat (KyHieu,Loai) values ('Na3PO4','MuoiT')
insert into Chat (KyHieu,Loai) values ('K3PO4','MuoiT')

insert into Chat (KyHieu,Loai) values ('Na2SO3','MuoiT')
insert into Chat (KyHieu,Loai) values ('K2SO3','MuoiT')
insert into Chat (KyHieu,Loai) values ('Li2SO3','MuoiT')

insert into Chat (KyHieu,Loai) values ('Na2S','MuoiT')
insert into Chat (KyHieu,Loai) values ('K2S','MuoiT')
insert into Chat (KyHieu,Loai) values ('Li2S','MuoiT')
insert into Chat (KyHieu,Loai) values ('CaS','MuoiT')
insert into Chat (KyHieu,Loai) values ('BaS','MuoiT')

insert into Chat (KyHieu,Loai) values ('Na2SiO3','MuoiT')
insert into Chat (KyHieu,Loai) values ('K2SiO3','MuoiT')
insert into Chat (KyHieu,Loai) values ('Li2SiO3','MuoiT')



/*Muoi khong tan*/
insert into Chat (KyHieu,Loai) values ('AgCl','MuoiKT')
insert into Chat (KyHieu,Loai) values ('AgBr','MuoiKT')
insert into Chat (KyHieu,Loai) values ('AgI','MuoiKT')

insert into Chat (KyHieu,Loai) values ('CuS','MuoiKT')
insert into Chat (KyHieu,Loai) values ('Ag2S','MuoiKT')
insert into Chat (KyHieu,Loai) values ('ZnS','MuoiKT')
insert into Chat (KyHieu,Loai) values ('HgS','MuoiKT')
insert into Chat (KyHieu,Loai) values ('MnS','MuoiKT')
insert into Chat (KyHieu,Loai) values ('Fe2S3','MuoiKT')
insert into Chat (KyHieu,Loai) values ('FeS','MuoiKT')

insert into Chat (KyHieu,Loai) values ('CuSO3','MuoiKT')
insert into Chat (KyHieu,Loai) values ('Ag2SO3','MuoiKT')
insert into Chat (KyHieu,Loai) values ('MgSO3','MuoiKT')
insert into Chat (KyHieu,Loai) values ('CaSO3','MuoiKT')
insert into Chat (KyHieu,Loai) values ('BaSO3','MuoiKT')
insert into Chat (KyHieu,Loai) values ('ZnSO3','MuoiKT')
insert into Chat (KyHieu,Loai) values ('HgSO3','MuoiKT')
insert into Chat (KyHieu,Loai) values ('MnSO3','MuoiKT')
insert into Chat (KyHieu,Loai) values ('FeSO3','MuoiKT')

insert into Chat (KyHieu,Loai) values ('CaSO4','MuoiKT')
insert into Chat (KyHieu,Loai) values ('BaSO4','MuoiKT')
insert into Chat (KyHieu,Loai) values ('Ag2SO4','MuoiKT')

insert into Chat (KyHieu,Loai) values ('Ag2CO3','MuoiKT')
insert into Chat (KyHieu,Loai) values ('MgCO3','MuoiKT')
insert into Chat (KyHieu,Loai) values ('CaCO3','MuoiKT')
insert into Chat (KyHieu,Loai) values ('BaCO3','MuoiKT')
insert into Chat (KyHieu,Loai) values ('ZnCO3','MuoiKT')
insert into Chat (KyHieu,Loai) values ('HgCO3','MuoiKT')
insert into Chat (KyHieu,Loai) values ('MnCO3','MuoiKT')
insert into Chat (KyHieu,Loai) values ('FeCO3','MuoiKT')

insert into Chat (KyHieu,Loai) values ('Li3PO4','MuoiKT')
insert into Chat (KyHieu,Loai) values ('Ca3(PO4)2','MuoiKT')
insert into Chat (KyHieu,Loai) values ('Ba3(PO4)2','MuoiKT')
insert into Chat (KyHieu,Loai) values ('AlPO4','MuoiKT')
insert into Chat (KyHieu,Loai) values ('CrPO4','MuoiKT')
insert into Chat (KyHieu,Loai) values ('Zn3(PO4)2','MuoiKT')
insert into Chat (KyHieu,Loai) values ('Fe3(PO4)2','MuoiKT')
insert into Chat (KyHieu,Loai) values ('FePO4','MuoiKT')
insert into Chat (KyHieu,Loai) values ('Mg3(PO4)2','MuoiKT')
insert into Chat (KyHieu,Loai) values ('Mn3(PO4)2','MuoiKT')
insert into Chat (KyHieu,Loai) values ('Cu3(PO4)2','MuoiKT')
insert into Chat (KyHieu,Loai) values ('Hg3(PO4)2','MuoiKT')

insert into Chat (KyHieu,Loai) values ('Ag2SiO3','MuoiKT')
insert into Chat (KyHieu,Loai) values ('MgSiO3','MuoiKT')
insert into Chat (KyHieu,Loai) values ('CaSiO3','MuoiKT')
insert into Chat (KyHieu,Loai) values ('BaSiO3','MuoiKT')
insert into Chat (KyHieu,Loai) values ('ZnSiO3','MuoiKT')
insert into Chat (KyHieu,Loai) values ('MnSiO3','MuoiKT')
insert into Chat (KyHieu,Loai) values ('FeSiO3','MuoiKT')
insert into Chat (KyHieu,Loai) values ('Fe2(SiO3)3','MuoiKT')
