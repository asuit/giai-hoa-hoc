#** Đồ Án Các Hệ Cơ Sở Tri thức *
##*Trường ĐH Công nghệ Thông tin - ĐHQG.TPHCM*

##1. Lưu Thanh Sơn - sonlam1102@yahoo.com.vn
##2. Đỗ Phú An - do.phuan@gmail.com

#**Cách tạo và push file lên git**
##**1. Khởi tạo git**
###*git init*
###*git remote add origin <đường dẫn trong mục HTTPS ở trang https://bitbucket.org/sonlam1102/giai-hoa-hoc/

##**2. Set thông tin cho git**
###*git fetch*
###*git checkout master*

##**3. Kiểm tra thông tin nhánh (branch)**
###*git branch*

##**4. Push code lên git hub **
###*git add -A*
###*git commit -m "Nội dung muốn ghi"*
###*git push origin master* 

#**Cách kéo (pull) file về từ git**
##**1. Khởi tạo git **
###Làm như mục 1,2 3 ở trên nếu mới khởi tạo git, nếu đã khởi tạo rồi thì bỏ qua mục khởi tạo

##**2. Pull code về máy từ git**
###*git pull*

##**5. Ignore một file nào đó**
###*git update-index --assume-unchanged "<path/to/file>"*